﻿using PeoplePets.Business.Result;
using PeoplePets.Data.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PeoplePets.Business.Interface
{
    public interface IPeoplePetService
    {
        Task<IEnumerable<PetByOwnerGender>> GetPetsByOwnerGender(string petType, string url);

        Task<IEnumerable<Person>> GetPeople(string url);
    }
}
