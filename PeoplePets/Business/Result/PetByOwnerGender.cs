﻿using PeoplePets.Data.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace PeoplePets.Business.Result
{
    public class PetByOwnerGender
    {
        public string Gender;
        public List<Pet> Pets;
    }
}
