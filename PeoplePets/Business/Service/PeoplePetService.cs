﻿using Newtonsoft.Json;
using PeoplePets.Business.Interface;
using PeoplePets.Business.Result;
using PeoplePets.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeoplePets.Business.Service
{
    public class PeoplePetService: IPeoplePetService
    {

        public async Task<IEnumerable<Person>> GetPeople(string url)
        {
            using (System.Net.Http.HttpClient client = new System.Net.Http.HttpClient())
            {
                var response = await client.GetAsync(url);
                if (response != null)
                {
                    var value = await response.Content.ReadAsStringAsync();
                    if (string.IsNullOrEmpty(value) == false)
                    {
                        var result = JsonConvert.DeserializeObject<List<Person>>(value);

                        if (result != null)
                        {
                            return result.OrderBy(s => s.Name).ToList();
                        }
                    }
                }
            }

            return new List<Person>();
        }

       
        public async Task<IEnumerable<PetByOwnerGender>> GetPetsByOwnerGender(string petType, string url)
        {
            var peopleContext = await GetPeople(url);

            if (peopleContext is List<Person>)
            {
                var resultPetResults = from p in (peopleContext as List<Person>)
                                       group p by p.Gender into g
                                       select new PetByOwnerGender()
                                       {
                                           Gender = g.Key,
                                           Pets = g.Any(s => s.Pets != null &&
                                           s.Pets.Any(d => d.Type.Equals(petType, StringComparison.OrdinalIgnoreCase))) ?
                                           g.Where(s => s.Pets != null)
                                           .SelectMany(s => s.Pets.Where(d => d.Type.Equals(petType, StringComparison.OrdinalIgnoreCase)))
                                           .OrderBy(x => x.Name).ToList() : null
                                       };
                return resultPetResults;
            }

            return new List<PetByOwnerGender>();
        }
    }
}
