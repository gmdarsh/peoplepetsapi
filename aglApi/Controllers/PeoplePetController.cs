﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PeoplePets.Business.Interface;

namespace aglApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PeoplePetController : ControllerBase
    {
        private readonly IPeoplePetService peoplePetService;
        private readonly IConfiguration configuration;
        private readonly ILogger<PeoplePetController> logger;
        public PeoplePetController(IPeoplePetService peoplePetService, 
            IConfiguration configuration,
            ILogger<PeoplePetController> logger)
        {
            this.peoplePetService = peoplePetService;
            this.configuration = configuration;
            this.logger = logger;
        }

        [HttpGet("GetCatsByOwnerGender")]
        public async Task<ActionResult> GetCatsByOwnerGender()
        {
            logger.LogInformation("GetCatsByOwnerGender api get started");
            var result = await this.peoplePetService.GetPetsByOwnerGender("Cat", configuration["DefaultUrl"]);
            logger.LogInformation("GetCatsByOwnerGender api get completed");
            return Ok(result);
        }
    }
}