﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AglApi.Helpers.Middleware
{
    public class CustomErrorResponse
    {
        public string Message { get; set; }
        public string Description { get; set; }
    }
}
