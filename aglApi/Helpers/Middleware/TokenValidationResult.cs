﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AglApi.Helpers.Middleware
{
    public struct TokenValidationResult
    {
        public bool IsValid { get; set; }

        public FailReason? Reason { get; set; }


        public TokenValidationResult(FailReason failReason)
        {
            IsValid = false;
            Reason = failReason;
        }

        public static TokenValidationResult Success => new TokenValidationResult { IsValid = true };

        public static TokenValidationResult Fail(FailReason reason) => new TokenValidationResult(reason);
    }

    public enum FailReason
    {
        Expired,
        InvalidSignature
    }
}
