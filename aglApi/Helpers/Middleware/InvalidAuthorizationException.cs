﻿using System;
using System.Runtime.Serialization;

namespace AglApi.Helpers.Middleware
{
    [Serializable]
    internal class InvalidAuthorizationException : Exception
    {
        public InvalidAuthorizationException()
        {
        }

        public InvalidAuthorizationException(string message) : base(message)
        {
        }

        public InvalidAuthorizationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidAuthorizationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}