﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AglApi.Helpers.Middleware
{
    public class RequestValidationMiddleware
    {
        private readonly RequestDelegate _next;
        private const string StringPropertiesPattern = "\"(?<key>\\w*)\":\\s*\"(?<value>\\S*?)\\\"";

        public RequestValidationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                var authHeader = context.Request.Headers[HeaderNames.Authorization];// context.Request.Headers.Where(x => x.Key == RequestHeaders.Authorisation);

                if (authHeader.Any())
                {
                    var parsed = AuthenticationHeaderValue.Parse(authHeader);

                    var token = parsed.Parameter;
                    var validation = ValidateToken(token);
                    if (!validation.IsValid)
                    {
                        throw new InvalidAuthorizationException();
                    }

                    var appId = GetStringProperty(token, "appId");
                    var principal = new AppPrincipal();
                    principal.App = new AuthenticatedApp { AppId = Guid.Parse(appId) };

                    context.User = new System.Security.Claims.ClaimsPrincipal(principal);

                    context.Response.OnStarting(state =>
                    {
                        var httpContext = (HttpContext)state;
                        if (httpContext.Response.Headers.ContainsKey("Access-Control-Expose-Headers") == false)
                            httpContext.Response.Headers.Add("Access-Control-Expose-Headers", "Authorization");

                        httpContext.Response.Headers.Add(HeaderNames.Authorization, GenerateNewToken(token));

                        return Task.CompletedTask;
                    }, context);
                }
                else
                {
                    //TO DO;
                    // thorw execption if authentication headers is not present
                    // throw new InvalidAuthorizationException();
                }

                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var response = context.Response;
            var customException = exception as BaseCustomException;
            var statusCode = (int)HttpStatusCode.InternalServerError;
            var message = "Unexpected error";
            var description = "Unexpected error";


            if (null != customException)
            {
                message = customException.Message;
                description = customException.Description;
                statusCode = customException.Code;
            }

            Log.Error(exception, message);

            response.ContentType = "application/json";
            response.StatusCode = statusCode;
            await response.WriteAsync(JsonConvert.SerializeObject(new CustomErrorResponse
            {
                Message = message,
                Description = description
            }));
        }

        private TokenValidationResult ValidateToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                throw new ArgumentException("message", nameof(token));
            }

            var parsedToken = ParseToken(token);

            var passwordHash = new PasswordHash();

            return TokenValidationResult.Success;
        }

        private string ParseToken(string token)
        {
            return token;
        }

        public string GetStringProperty(string token, string propertyName)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new ArgumentException("message", nameof(token));
            }

            var properties = GetTokenProperties(token, StringPropertiesPattern);
            properties.TryGetValue(propertyName, out var value);
            return value;
        }

        private Dictionary<string, string> GetTokenProperties(string token, string pattern)
        {
            var parsed = ParseToken(token);
            if (parsed == null)
            {
                throw new InvalidOperationException("Error parsing token - returned null");
            }

            var result = GetProperties(parsed, pattern);

            return result;
        }

        private Dictionary<string, string> GetProperties(string payload, string pattern)
        {
            var result = new Dictionary<string, string>();

            var matches = Regex.Matches(payload, pattern, RegexOptions.Compiled);

            foreach (Match m in matches)
            {
                if (m.Success)
                {
                    var key = m.Groups["key"]?.Value;
                    var value = m.Groups["value"]?.Value;
                    if (key != null)
                    {
                        result.Add(key, value);
                    }
                }
            }

            return result;
        }

        private string GenerateNewToken(string payload)
        {
            const string StringPropertiesPattern = "\"(?<key>\\w*)\":\\s*\"(?<value>\\S*?)\\\"";
            var properties = new Dictionary<string, string>();

            var matches = Regex.Matches(payload, StringPropertiesPattern, RegexOptions.Compiled);
            foreach (Match m in matches)
            {
                if (m.Success)
                {
                    var key = m.Groups["key"]?.Value;
                    var value = m.Groups["value"]?.Value;
                    if (key != null)
                    {
                        properties.Add(key, value);
                    }
                }
            }

            int.TryParse(properties.ContainsKey("TokenDuration") ? properties["TokenDuration"] : "15", out var TokenDuration);
            if (TokenDuration < 1) TokenDuration = 1;



            return CreateSecurityToken();
        }

        string CreateSecurityToken()
        {
            return Guid.NewGuid().ToString();
        }
    }    

    public static class RequestValidationMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestValidationMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestValidationMiddleware>();
        }
    }
}
