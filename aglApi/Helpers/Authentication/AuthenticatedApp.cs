﻿using System;

namespace AglApi.Helpers.Middleware
{
    public class AuthenticatedApp
    {
        public Guid AppId { get; set; }
    }
}