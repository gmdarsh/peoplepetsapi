﻿using System;
using System.Security.Principal;

namespace AglApi.Helpers.Middleware
{
    public interface IAppPrincipal : IPrincipal
    {
        AuthenticatedApp App { get; set; }
        Guid ClientIdentifer { get; }
        DateTimeOffset? Date { get; }
    }
}