﻿using System;
using System.Security.Principal;

namespace AglApi.Helpers.Middleware
{
    public class AppPrincipal : IAppPrincipal
    {
        public AuthenticatedApp App { get; set; }
        public Guid ClientIdentifer { get; }
        public DateTimeOffset? Date { get; }

        public IIdentity Identity
        {
            get
            {
                return new GenericIdentity(App.AppId.ToString(), "AppId");
            }
        }

        public bool IsInRole(string role)
        {
            return true;
        }
    }
}