using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using PeoplePets.Business.Interface;
using PeoplePets.Business.Result;
using PeoplePets.Business.Service;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using PeoplePets.Data.Model;

namespace Tests
{
    public class PeoplePetServiceTest
    {
        IConfiguration configuration;
        IPeoplePetService peoplePetService;
        const int mockPeopleCount = 6;
        const int mockCatsCount = 7;
        List<PetByOwnerGender> mockObject;
        public List<PetByOwnerGender> GetMockObject()
        {
            var mockObject = new List<PetByOwnerGender>();
            mockObject.Add(new PetByOwnerGender
            {
                Gender = "Male",
                Pets = new List<Pet>()
            {
                new Pet() { Name = "Garfield", Type ="Cat" },
                new Pet() { Name = "Jim", Type ="Cat" },
                new Pet() { Name = "Max", Type ="Cat" },
                new Pet() { Name = "Tom", Type ="Cat" }
            }
            });
            mockObject.Add(new PetByOwnerGender
            {
                Gender = "Female",
                Pets = new List<Pet>()
            {
                new Pet() { Name = "Garfield", Type ="Cat" },
                new Pet() { Name = "Simba", Type ="Cat" },
                new Pet() { Name = "Tabby", Type ="Cat" }
            }
            });
            return mockObject;
        }



        [SetUp]
        public void Setup()
        {
            this.configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            this.peoplePetService = new PeoplePetService();
            this.mockObject = this.GetMockObject();
        }

        [Test]
        public async Task GetPeopleList()
        {           
            var peopleList = await peoplePetService.GetPeople(configuration["DefaultUrl"]);
            Assert.IsNotNull(peopleList);
        }

        [Test]
        public async Task GetPeople_ValidateCount()
        {
            var peopleList = await peoplePetService.GetPeople(configuration["DefaultUrl"]);
            Assert.AreEqual(peopleList.Count(), mockPeopleCount);
        }

        [Test]
        public async Task GetPetCatByOwnerGender_ValidateSequence()
        {
            var petByOwnerList = await peoplePetService.GetPetsByOwnerGender("Cat", configuration["DefaultUrl"]);
            var maleCats = petByOwnerList.FirstOrDefault(s => s.Gender == "Male").Pets.Select(s=>s.Name);
            var expectedMaleCats = mockObject.FirstOrDefault(s => s.Gender == "Male").Pets.OrderBy(s => s.Name).Select(s=>s.Name);

            var femaleCats = petByOwnerList.FirstOrDefault(s => s.Gender == "Female").Pets.Select(s => s.Name);
            var expectedFemaleCats = mockObject.FirstOrDefault(s => s.Gender == "Female").Pets.OrderBy(s=>s.Name).Select(s => s.Name);

            Assert.IsTrue(expectedMaleCats.SequenceEqual(maleCats));
            Assert.IsTrue(expectedFemaleCats.SequenceEqual(femaleCats));
        }
    }
}